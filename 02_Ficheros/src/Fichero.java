import java.awt.EventQueue;
import java.io.File;
import java.io.IOException;

public class Fichero {
 

	public static void main(String[] args) throws IOException {

		File carpeta = new File("FICHEROS");
				
	
		if(carpeta.mkdir() == true){
			System.out.println("Carpeta creada");
	
			File fich1 = new File(carpeta, "primero.txt"),
				 fich2 = new File(carpeta, "**********.txt");
	
			try {
		
				fich1.createNewFile();
				fich2.createNewFile();
	
			} catch (Exception e) { 
				System.out.println(e.getMessage());
			}
		}
		else {
			System.out.println("Carpeta no creada");
		}
   }
}
