// ConsoleApplicationFicheroPersonas.cpp: define el punto de entrada de la aplicaci�n de consola.
//

#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define D 10
#define CU 40
#define CI 100


enum Opciones { // enumeraci�n de la opciones

	cargar_datos,
	escribir_tabla,
	escribir_datos,
	insertar_datos,
	salir,
	OPCIONES

};

struct TAlumno {

	char Nombre[CU];
	int edad;

}TablaAlumno;

const char *texto[] = { // declaraci�n de las opciones

	"Cargar los datos",
	"Escribir en la tabla",
	"Escribir un dato",
	"Insertar un dato",
	"Salir",

};

void menu() { // declaraci�n del men�

	printf("\t\t OPCIONES PARA TABLA\n");
	printf("\t\t========================================\n");
	for (int i = 0; i<OPCIONES; i++)
		printf("\t\t%i. %s\n", i, texto[i]);


}

void CargarDatos(char NombreFichero[50], struct TAlumno TablaAlumno[D], int *numero) { // DECLARACI�N CARGAR DATOS

	struct TAlumno;
	char nombreAlumno[CU];
	int edadAlumno, posicion;
	FILE *pf;

	posicion = 0;
	(*numero) = 0;

	pf = fopen(NombreFichero, "rt");
	if (pf != NULL) {
		while (!feof(pf))
		{

			fscanf(pf, "%s", nombreAlumno);
			fscanf(pf, "%i", &edadAlumno);
			strcpy(TablaAlumno[posicion].Nombre, nombreAlumno);
			TablaAlumno[posicion].edad = edadAlumno;
			posicion++;

		}
		(*numero) = posicion;
		fclose(pf);
	}
}

void GuardarDatos(char NombreFichero[50], struct TAlumno TablaAlumno[D], int numero){
	FILE *pf;
	int posicion;

	pf = fopen(NombreFichero, "wt");
	if (pf != NULL) {
		for (posicion = 0; posicion < numero; posicion++) {
			fprintf(pf, "%s\n", TablaAlumno[posicion].Nombre);
			fprintf(pf, "%i\n", TablaAlumno[posicion].edad);
		}
		fclose(pf);
	}



}

/*void EscribirDatos(char NombreFichero[50], struct TAlumno TablaAlumno[D]) { // DECLARACI�N ESCRIBIR DATOS

	struct TAlumno;
	int i = 0;
	FILE *pf;

	pf = fopen(NombreFichero, "rt");
	if (pf != NULL) {

	while ((i = getc(pf)) != EOF) {
		printf("Dime tu nombre: ");
		fgets(TablaAlumno->Nombre, 40, stdin);

		printf("Introduce la edad: ");
		fgets((char*)TablaAlumno->edad, 3, stdin);

		i++;
	}

	fwrite(TablaAlumno, sizeof(TablaAlumno), 10, pf);


	fclose(pf);


}*/

/*void InsertarDatos(char NombreFichero[50], TAlumno TablaAlumno[D]) { // DECLARACION INSERTAR DATOS

	struct TAlumno;
	char buffer[CI];
	FILE *pf;

	fprintf(pf, buffer);
	fprintf(pf, "%s", "\nDiego.");

}*/

int main(int argc, char *argv[]) {

	struct TAlumno TablaAlumno[10];
	int opcion;
	int numero;

	printf("\n");

	do {

		menu();
		printf("\t\t Opci�n a realizar: ");
		scanf("%i", &opcion);

		switch (opcion) {

		case 0:
			CargarDatos("algo.txt", TablaAlumno, &numero);
			break;

		case 1:
			GuardarDatos("algo3.txt", TablaAlumno, numero);
			break;

		case 2:
			//EscribirDatos("algo.txt", TablaAlumno);;
			break;

		case 3:
			return EXIT_SUCCESS;
			break;

		case 4:
			printf("Adi�s\n");
			return EXIT_SUCCESS;
			break;

		default:
			printf("Opci�n incorrecta. Prueba otra vez");
			break;
		}

	} while (opcion != 6);

	return EXIT_SUCCESS;
}





/*

Gestionar datos alumno

Nombre de 40 caracteres

edad

10 alumnos

1. Cargar datos

2. Escribir datos

3. insertar en la tabla

4. Escribir la tabla en la pantalla

5. borrar un dato de la tabla

6. Salir.
*/
